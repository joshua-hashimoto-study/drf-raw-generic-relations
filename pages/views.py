from rest_framework.generics import ListCreateAPIView

from .serializers import BookmarkSerializer, NoteSerializer, TaggedItemSerializer
from .models import Bookmark, Note, TaggedItem


class BookmarkListCreateAPIView(ListCreateAPIView):
    queryset = Bookmark.objects.all()
    serializer_class = BookmarkSerializer


class NoteListCreateAPIView(ListCreateAPIView):
    queryset = Note.objects.all()
    serializer_class = NoteSerializer


class TaggedItemListCreateAPIView(ListCreateAPIView):
    queryset = TaggedItem.objects.all()
    serializer_class = TaggedItemSerializer
