from django.urls import path

from .views import BookmarkListCreateAPIView, NoteListCreateAPIView, TaggedItemListCreateAPIView

app_name = 'pages'

urlpatterns = [
    path('bookmark/', BookmarkListCreateAPIView.as_view(), name='bookmark'),
    path('note/', NoteListCreateAPIView.as_view(), name='note'),
    path('tags/', TaggedItemListCreateAPIView.as_view(), name='tags'),
]
