from rest_framework import serializers

from .models import TaggedItem, Bookmark, Note


class TaggedObjectRelatedField(serializers.RelatedField):
    """
    A custom field to use for the `tagged_object` generic relationship.
    """

    def to_representation(self, value):
        """
        Serialize tagged objects to a simple textual representation.
        """
        print(value)
        if isinstance(value, Bookmark):
            return 'Bookmark: ' + value.url
        elif isinstance(value, Note):
            return 'Note: ' + value.text
        raise Exception('Unexpected type of tagged object')


class TaggedItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = TaggedItem
        fields = '__all__'


class BookmarkSerializer(serializers.ModelSerializer):
    tagged_item = TaggedItemSerializer(many=True)
    tags = TaggedObjectRelatedField(
        queryset=Bookmark.objects.all(), required=False, allow_null=True, source='tagged_item')

    class Meta:
        model = Bookmark
        fields = (
            'id',
            'url',
            'tags',
        )


class NoteSerializer(serializers.ModelSerializer):
    tagged_item = TaggedItemSerializer(many=True)
    tags = TaggedObjectRelatedField(
        queryset=Note.objects.all(), required=False, allow_null=True, source='tagged_item')

    class Meta:
        model = Note
        fields = (
            'id',
            'text',
            'tags',
        )
